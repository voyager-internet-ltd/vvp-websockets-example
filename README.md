# Websocket Examples

To run and test the example apps, go into each app directory and refer to the simple startup guide.

The following example apps currently exist:

- [javascript](javascript)
- [python](python)

## Python

### Requirements
 - Python 3
 - requests package
 - websocket package 
 If lacking any of these, use 'pip install <packagename>' to download the package
 
### Setup/Configuration
 
With websockets, you may be required to perform HTTP authentication before
connecting to the websocket endpoint.
 
If this is the case, you'll need to update the configuration variable
authCredentials in the example_websocket.py file:
 
Similarly, you'll need to update the account name for Kazoo - accountName.

The authEndpoint is the https endpoint for web-based authentication.
websocketListEndpoint is the endpoint for getting a list of all available
websockets for subscription.

websocketAddress is the primary websocket endpoint.

These will need to be updated with your environment.

### Examples

The example code shows various stages of connection, including:
 - configuration
 - where credentials are obtained
 - when the http authorisation and token is received
 - an example of getting all the available websockets
 - how to subscribe to a channel binding
 - an example of how to listen on a websocket for events

The recommendation would likely to be to put the listening into a threaded loop or worker class as part of your solution.
The output could either be printed (as in the example) or passed to say, a Kafka producer for a Kafka stream, or an alternative method of logging.

### Example of output of authentication, connecting, getting a list of channels, subscribing and receiving

![Output from example python](/images/python_example_output.jpg "Example output")


## Javascript

This is an example of a browser-based javascript implementation of a websocket client.

This is useful, as you really only need a hostname, port and the messages to send.

### Fields:
 - WS Protocol - wss or ws depending on your host
 - WS Hostname - eg apps01.dev.voice.isx.nz
 - WS Port - usually 5443
 - WS Endpoint - if required
 
 Message: the message being sent to the server.  In this example, we have an 
 auth token from http authentication, and send a subscribe for CHANNEL_CREATE
 messages, for all account ids.
 
 ```json
{
    'action': 'subscribe',
    'auth_token': 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjdjMDFhN2EzY2ExNjAyOTUxY2I2YTU3MDYxMjk3YmUzIn0.eyJpc3MiOiJrYXpvbyIsImlkZW50aXR5X3NpZyI6InJUTTN2ZFV5dktCWldMUDZFNGxFUEpEYTUwWEF2UDBvOUwtZXlJRjMtajAiLCJhY2NvdW50X2lkIjoiOWZiZWE0NTc4MzFhZmNhOWNmZDQzNDU4YWYzMTAyMWQiLCJvd25lcl9pZCI6IjlmYWVjYTU3ZmYzOTY2MzNkYTM3NzdkNjJiNTQ0NWFlIiwibWV0aG9kIjoiY2JfdXNlcl9hdXRoIiwiZXhwIjoxNjA3NTEyMTE5fQ.Ikp8lKqOpm17qgbnSLuwOimADLCdpAuJTSGo2ryIq7rfGeDV2epeqMu0tiMenEtebErjTIZ6E2G-8lf-Xhb_NGdnf7fKRnw0pk9q27vFPQOvbb3I4nedVEEbaP7pK1Y3APXpbaUWo85j2q4pMo7GE0VJcjQMzID7iNu5hAbQEAoXDEcDT_J0_TWxAMhDaf1wFySgaOgF4S5xywp4bOZy8_tBQMdP8SwCfZ_lrKToOWjBwqWAMaAwnF34ifNyM-6trXUIgEMukG5JtLdoV_EK5R-FqlP2Rk7n_WvxugMD6y9_-ttbNfsF-OS7nMl8UVpP8Kr8zY5Vb7C3k7x9ltUXyg',
    'data': 
	{
		'account_id': '*',
		'binding': 'call.CHANNEL_CREATE.*'
	}
}
 ```
 
 ### Screenshot of results
 
 ![Output from example javascript client](/images/javascript_client_example.png "Example output")

In the debug box, you can see the response messages, as well as in the 
browser inspector (in this case, Firefox).

Another advantage of Firefox is the Websocket inspector.  In Developer Tools,
go to Network, then filter by WS:

 ![Seeing the websocket inspector](/images/ws_inspector.png "Example of using Firefox's websocket inspector")
