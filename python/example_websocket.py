import requests, json, hashlib
from websocket import create_connection

import http.client as http_client
http_client.HTTPConnection.debuglevel = 1


#configuration
authCredentials = 'username@password'
accountName = 'admin'
authEndpoint = 'https://server:8443/v2/user_auth'
websocketListEndpoint = 'https://server:8443/v2/websockets'
websocketAddress = 'wss://server:5443/'

#obtaining credentials
credentials = hashlib.md5(authCredentials.encode('utf-8')) 
print(credentials.hexdigest())

data = {'credentials': credentials,
    'account_name':accountName,
    'method':'md5' }
    
data = {'data':{'credentials': credentials.hexdigest(), 'account_name' : accountName }}    


#http authorisation
r = requests.put(authEndpoint, data=(json.dumps(data)),  headers={'Content-type': 'application/json; charset=utf-8'}, verify=False)
print(r)

accountid = json.loads(r.text)['data']['account_id']
token = json.loads(r.text)['auth_token'] #httpauthtoken

print("TOKEN: " + token)

r = requests.get(websocketListEndpoint)
print(r.text)
parsed = json.loads(r.text)
print(json.dumps(parsed, indent=4, sort_keys=True))


#subscribe to a binding
data = {'action':'subscribe', 'auth_token':token, 'data':{'account_id':'*', 'binding':'call.CHANNEL_CREATE.*'}}
print(data)
ws = create_connection(websocketAddress)
ws.send(json.dumps(data))
print ("sent")

#listen for and receive info on a websocket for the bindings specified (if executed after the subscription above, you'll get the success message for subscribing)
result = ws.recv()
print ("received: " + json.dumps(result))
